package edu.ucsd.cse110.library.rules;

import edu.ucsd.cse110.library.MemberType;


public class StudentLateSummerAssessor implements Assessor {

	@Override
	public boolean evaluate(Properties prop) {
		//In summer students can keep books for 1 month
		if (prop.getType() == MemberType.Student && 
				prop.getCheckoutDate().getMonthValue()>6 &&
					prop.getCheckoutDate().getMonthValue()<9) 
				return prop.getDays()>30;			
		return false;
	}

	@Override
	public String getErrors() {
		return null;
	}

}
